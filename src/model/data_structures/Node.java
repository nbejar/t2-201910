package model.data_structures;

public class Node<T> extends IteratorLista
{

	//------------------------------------------------------
	//Atributos  
	//------------------------------------------------------

	/**
	 * elemento del nodo
	 */
	private T elemento;

	/**
	 * Nodo siguiente
	 */
	private Node<T> siguiente;

	//------------------------------------------------------
	//Metodos  
	//------------------------------------------------------
	
	/**
	 * Metodo constructor
	 * @param lista
	 */
	public Node(LinkedList<T> lista) {
		super(lista);
		// TODO Auto-generated constructor stub
	}


	/**
	 * Este metodo retorna el nodo siguiente
	 * @return el nodo siguiente
	 */
	public Node<T> darSiguiente()
	{
		return siguiente;
	}

	/**
	 * Retorna el elemento del nodo
	 * @return el elemnto del nodo
	 */
	public T darElemento()
	{
		return elemento;
	}

	/**
	 * Cambiar el nodo siguiente
	 */
	public void cambiarSiguiente(T nuevo)
	{
		siguiente = (Node<T>) nuevo;
	}
	
	/**
	 * Este metodo retorna si tiene siguiente
	 */
	public boolean hasNext() {
		boolean rta = false;
		if(siguiente != null)
			rta = true;
		return rta;

	}
	
	/**
	 * Retorna el siguiente
	 */
	public Object Next()
	{
		return siguiente;
	}

	/**
	 * Se elemina el elemento de la lista y con el todos los eslementos siguiente
	 */
	public void remove() 
	{
		elemento = null;
	}
}
