package model.data_structures;

public interface Iterator<T> 
{
	/**
	 * Revisa que si tenga siguiente
	 * @return
	 */
	public boolean hasNext();
	
	/**
	 * Retorna el siguienete
	 * @return
	 */
	public T Next();
	
	/**
	 * elimina el nodo actual
	 */
	public void remove();
}
