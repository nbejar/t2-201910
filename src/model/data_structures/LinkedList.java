package model.data_structures;

import java.util.Iterator;

public class LinkedList<T> implements ILinkedList<T> 
{
	
	private Node primero;
	

	@Override
	public boolean add(T dato) 
	{
		Node actual = (Node) dato;
		if(primero != null)
			actual.cambiarSiguiente(primero);
		primero = actual;
		return true;

	}

	/**
	 * Este emtodo agrega el dato al final
	 */
	public boolean addAtEnd(T dato) 
	{
		boolean respuesta = false;
		Node actual = primero;
		if(actual!= null)
		{
			while(actual.hasNext())
				actual = actual.darSiguiente();
			actual.cambiarSiguiente(dato);
		}
		else
		{
			primero = (Node) dato;
		}
		return respuesta;
	}

	public T getElement() 
	{
		return primero;
	}

	@Override
	public T getCurrentElement() {
		return (T) primero;
	}


	public Integer getSize() 
	{
		int contador = 0;
		Node actual = primero;

		while(actual != null )
		{
			contador ++;
			actual = actual.darSiguiente();
		}
		return contador;
	}



	@Override
	public T delete(T dato) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean deleteAtK(int k) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public T next() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public T previous() {
		// TODO Auto-generated method stub
		return null;
	}
	

	@Override
	public model.data_structures.Iterator iterator() {
		// TODO Auto-generated method stub
		return null;


}
