package model.data_structures;

/**
 * Abstract Data Type for a linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface ILinkedList<T> extends Iterable 
{
	/**
	 * Agregar un dato de forma compacta (en la primera casilla disponible) 
	 * Caso Especial: Si el arreglo esta lleno debe aumentarse su capacidad, agregar el nuevo dato y deben quedar multiples casillas disponibles para futuros nuevos datos.
	 * @param dato nuevo elemento
	 * @return true si se logra agregar el elemento, false de lo contrario.
	 */
	public boolean add( T dato );

	/**
	 * Agregar un dato de forma compacta al final del arreglo 
	 * Caso Especial: Si el arreglo esta lleno debe aumentarse su capacidad, agregar el nuevo dato y deben quedar multiples casillas disponibles para futuros nuevos datos.
	 * @param dato nuevo elemento
	 * @return true si se logra agregar el elemento, false de lo contrario.
	 */
	public boolean addAtEnd( T dato );

	/**
	 * Retorna el elemento  necesitado
	 * @return el elemento solicitado
	 */
	public T getElement();

	/**
	 * Devuelve el elemento actual en donde esta ubicado
	 * @return el elemento de tipo T
	 */
	public T getCurrentElement();

	/**
	 * Retorna el tama�o del arreglo
	 * @return
	 */
	Integer getSize();

	/**
	 * Eliminar un dato del arreglo.
	 * Los datos restantes deben quedar "compactos" desde la posicion 0.
	 * @param dato Objeto de eliminacion en el arreglo
	 * @return dato eliminado
	 */
	T delete( T dato );


	/**
	 * Eliminar un dato del arreglo en una posicion determinada k
	 * Los datos restantes deben quedar "compactos" desde la posicion 0.
	 * @param indice k de posicion en el arregle 
	 * @return dato eliminado
	 */
	public boolean deleteAtK( int k );

	/**
	 *Retorna el elemento siguiente al elemento actual 
	 * @return
	 */
	T next();

	/**
	 * Retorna el elemento anterior al elemento actual
	 * @return
	 */
	T previous();


}
