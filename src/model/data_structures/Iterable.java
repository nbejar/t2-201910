package model.data_structures;

public interface Iterable <T>
{
	/**
	 * Este metodo hace el iterable 
	 * @return un elemento de tipo Iterator
	 */
	public Iterator<T> iterator();
	
}
